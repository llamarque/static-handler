package fr.edu.acdijon.client;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Log4J2LoggerFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.handler.StaticHandler;

public class MainVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(MainVerticle.class);

    private static final String HOST = "0.0.0.0";
    private static final int PORT = 8080;

    private static final String RANGE = "bytes=%d-%d";
    private static final int RANGE_SIZE = 1 * 1024;
    private static final Pattern CONTENT_RANGE_PATTERN = Pattern.compile("^bytes (\\d+)-(\\d+)/(\\d+)$");

    private static final boolean LOG_SERVER = false;
    private static final boolean LOG_CLIENT = true;

    private WebClient client;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        InternalLoggerFactory.setDefaultFactory(Log4J2LoggerFactory.INSTANCE);

        client = WebClient.create(
            vertx,
            new WebClientOptions()
                .setLogActivity(LOG_CLIENT));

        Router router = Router.router(vertx);
        router.get("/file/:name").handler(this::getFile);
        router.route().handler(StaticHandler.create().setWebRoot("."));

        vertx.createHttpServer(new HttpServerOptions().setLogActivity(LOG_SERVER))
            .requestHandler(router::accept)
            .listen(PORT, HOST, listenHandler -> {
                startFuture.handle(listenHandler.mapEmpty());
            });
    }

    private void getFile(RoutingContext context) {
        Path path = Paths.get(context.pathParam("name"));

        client.head(PORT, HOST, "/" + path)
            .send(headHandler -> {
                if (headHandler.succeeded()) {
                    logger.info("File size = {}", Integer.valueOf(headHandler.result().getHeader("Content-Length")));

                    getChunk(path, 0);
                } else {
                    logger.error("=== HEAD ===============> ", headHandler.cause());
                }
            });

        context.response()
            .setStatusCode(204)
            .end();
    }

    private void getChunk(Path path, int offset) {
        client.get(PORT, HOST, "/" + path)
            .putHeader("Range", String.format(RANGE, offset, offset + RANGE_SIZE - 1))
            .send(getHandler -> {
                if (getHandler.succeeded()) {
                    MultiMap headers = getHandler.result().headers();

                    // logger.info("=======================================");
                    // logger.info("HTTP version = {}", getHandler.result().version());
                    // headers.names().forEach(key -> {
                    //     headers.getAll(key).forEach(value -> logger.info("Header: {} = {}", key, value));
                    // });
                    // logger.info("Body: \n{}", getHandler.result().bodyAsString());

                    Matcher matcher = CONTENT_RANGE_PATTERN.matcher(headers.get("Content-Range"));
                    if (matcher.matches()) {
                        int start = Integer.valueOf(matcher.group(1));
                        int end = Integer.valueOf(matcher.group(2));
                        int size = Integer.valueOf(matcher.group(3));

                        logger.info("=== CHUNK ==============> {}-{}/{}", start, end, size);

                        vertx.fileSystem().open("dl_" + path, new OpenOptions(), openHandler -> {
                            if (openHandler.succeeded()) {
                                AsyncFile file = openHandler.result();

                                file.write(getHandler.result().bodyAsBuffer(), start, handler -> {
                                    if (end + 1 < size) {
                                        getChunk(path, end + 1);
                                    }
                                });
                            } else {
                                logger.error("=== FILE ===============> ", openHandler.cause());
                            }
                        });
                    }
                } else {
                    logger.error("=== GET ================> ", getHandler.cause());
                }
            });
    }
}
